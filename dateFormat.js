// 5. Make the registered date into the DD/MM/YYYY format.

const friendsData = require('./arrays-people-friends.js');

function dateFormat(date)
{
    date["registered"] = date["registered"].substring(0, date["registered"].indexOf("T") - 1);
    console.log(date["registered"]);
    date["registered"] = date["registered"].split('-').reverse().join('/');
    return date;
}

const result = friendsData.map(dateFormat);
console.log(result);