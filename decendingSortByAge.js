// 2. Sort by descending order of age.

const friendsData = require('./arrays-people-friends.js');

const result = friendsData.sort(function(value1, value2)
{
    if(value1.age < value2.age)
    {
        return 1;
    }
    else if(value1.age > value2.age)
    {
        return -1;
    }
    else
    {
        return 0;
    }
});

console.log(result);