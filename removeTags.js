// 4. Remove the tags key.

const friendsData = require('./arrays-people-friends.js');

function removeTags(tagKey)
{
    delete tagKey["tags"];
    return tagKey;
}

const result = friendsData.map(removeTags);
console.log(result);