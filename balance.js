// 1. Make the balance into a number. If the number is invalid, default to 0. Do not overwrite the existing key, create a new one instead.

const friendsData = require('./arrays-people-friends.js');

function updateBalance(value)
{
    value["balance"] = Number(value["balance"].replace('$','').replace(',',''));
    console.log(typeof NaN);
    if(!isNaN(value["balance"]))
    {
        value["Updated Balance"] = value["balance"];
    }
    else
    {
        value["Updated Balance"] = 0;
    }
    return value;
}

const result = friendsData.map(updateBalance);
module.exports = result;
console.log(result);


