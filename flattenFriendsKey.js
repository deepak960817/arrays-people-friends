// 3. Flatten the friends key into a basic array of names.

const friendsData = require('./arrays-people-friends.js');

//console.log(friendsData);
function flatten(flatFriends)
{
    const friend = flatFriends["friends"].map(function(people)
    {
        delete people["id"];
        return people["name"];
    });
    flatFriends["friends"] = friend;
    return flatFriends;
}

const result = friendsData.map(flatten);
console.log(result);